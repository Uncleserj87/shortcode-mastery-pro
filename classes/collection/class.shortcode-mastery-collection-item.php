<?php
/**
 * Shortcode Mastery Collection Item
 *
 * @class   Shortcode_Mastery_Collection_Item
 * @package Shortcode_Mastery
 * @version 2.0.0
 *
 */

class Shortcode_Mastery_Collection_Item {
	
	/**
	 * Requested data
	 *
	 * @access protected
	 * @var array
	 */
	
	protected $data;
	
	/**
	 * Menu permission
	 *
	 * @access private
	 * @var string
	 */
	
	protected $menu_permission = 'manage_options';
	
	/**
	 * Constructor
	 *
	 * @param array $data Shortcode data
	 * @param string $tab Current tab
	 * @param string $tag Current tag
	 * @param int $paged Current page
	 * @param string $s Current search term
	 * @param bool $disabled Already added
	 */

	public function __construct( $data, $tab = 'all', $tag = '', $pack = '', $paged = 1, $s = '', $disabled = 'false' ) {
		
		$this->data = $data;
		
		$this->render( $tab, $tag, $pack, $paged, $s, $disabled );
				
	}
	
	/**
	 * Render collection item
	 *
	 * @param string $tab Current tab
	 * @param string $tag Current tag
	 * @param string $tag Current pack
	 * @param int $paged Current page
	 * @param string $s Current search term
	 * @param bool $disabled Already added
	 */
	
	public function render( $tab, $tag, $pack, $paged, $s, $disabled ) {
		
		$url = add_query_arg( array(
		    'action'    => 'shortcode-mastery-details-page',
		    'new_id'    => $this->data['id'],
		    'tag'       => $tag,
		    'tab'       => $tab,
		   	'pack'      => $pack, 
		    's'         => $s,
		    'paged'     => $paged,
		    'disabled'  => $disabled,
		    'sm_nonce'  => wp_create_nonce( 'shortcode-mastery-details-page-' . absint( $this->data['id'] ) ),
		    'TB_iframe' => 'true',
		    'width'     => '722',
		    'height'    => '620'
		), admin_url( 'admin.php' ) );
		
		?>
		
		<div class="sm-collection-item">
			
			<?php $this->render_badge_left(); ?>
			<?php $this->render_badge_right(); ?>

			<div class="info">
			
				<div class="image-holder loading">
					<?php if ( $this->data['content']['rendered'] ) { ?>
					<a href="<?php echo $url; ?>" class="thickbox image-thick">
						<?php $this->render_image(); ?>
					</a>
					<?php } else {
						$this->render_image();
					} ?>
				</div>
		
				<h3>
					<strong><?php echo $this->data['title']['rendered']; ?></strong>
				</h3>
				<?php if ( current_user_can ( $this->menu_permission ) ) { ?>	    	
		    	<a class="sm-item-button" href="<?php echo wp_nonce_url( admin_url('admin.php?page=shortcode-mastery-templates') . '&amp;new_id=' . $this->data['id'] . '&amp;tab=' . $tab . '&amp;tag=' . $tag . '&amp;pack=' . $pack . '&amp;s=' . $s . '&amp;paged=' . $paged . '&amp;disabled=' . $disabled, 'add_shortcode_with_id_' . $this->data['id'], 'sm_nonce' ); ?>" title="<?php _e( 'Add shortcode', 'shortcode-mastery' ); ?>" data-id="<?php echo $this->data['id']; ?>"> 
			    	<span class="plus">+</span>
					<span class="add"><?php _e( 'Add', 'shortcode-mastery' ); ?></span>	
				</a>
				<?php } ?>
				<div class="desc">
					<?php echo $this->data['excerpt']['rendered']; ?>
				</div>
				
				<?php if ( $this->data['content']['rendered'] ) { ?>
				
				<a href="<?php echo $url; ?>" class="thickbox details" style="margin-left: 18px"><?php _e( 'More Details', 'shortcode-mastery' ); ?></a>
				
				<?php } ?>
								
				<?php $this->render_packs(); ?>
				<?php $this->render_tags(); ?>
			</div>
		
		</div>		
		
	<?php 
	}
	
	/**
     * Get image source from request
     *
     * @return string Image Source (Large or Full size)
     */
		
	protected function get_image_source() {
		
		$source = null;
				
		if ( isset( $this->data['_embedded'] ) ) {
			
			$embedded = $this->data['_embedded'];
			
			$embedded = (array) $embedded;
			
			if ( isset($embedded['wp:featuredmedia']) && sizeof( $embedded['wp:featuredmedia'] ) > 0 ) {
			
				$sizes = $embedded['wp:featuredmedia'][0]['media_details']['sizes'];
			
				if ( isset( $sizes['shortcode'] ) ) {
					
					$source = $sizes['shortcode']['source_url'];
					
				} else {
					
					$source = $sizes['full']['source_url'];
				}
			
			}
												
		}
		
		return $source;
	}
	
	/**
     * Render thumbnail
     */
	
	protected function render_image() {
		
		$source = $this->get_image_source();
		
		if ( $source ) {
			
			echo '<img class="sm-lazy" data-original="' . $source . '" />';
			
		} else {
	
			echo '<img class="default" src="' . SHORTCODE_MASTERY_URL . 'images/sm-bigger@2x.png' . '" />';
			
		}
		
	}
	
	/**
     * Render tags
     */
	
	protected function render_tags() {
		
		$tags = $this->get_tax_terms( 'subtag' );
		
		if ( $tags ) { ?>
		<div class="params">
			<span class="sm-param-caption"><?php _e( 'Tags:', 'shortcode-mastery' ); ?></span>
			<?php
			foreach ( $tags as $tag ) { ?>
				<a href="<?php echo admin_url('admin.php?page=shortcode-mastery-templates'); ?>&amp;tag=<?php echo $tag['slug']; ?>" <span class="sm-inline-attr sm-inline-param sm-inline-button" data-tag="<?php echo $tag['slug']; ?>"><?php echo esc_html( $tag['name'] ); ?></span></a>
			<?php } ?>
		</div>
		<?php }

	}
	
	/**
     * Render tags
     */
	
	protected function render_packs() {
		
		$packs = $this->get_tax_terms( 'pack' );
		
		if ( $packs ) { ?>
		<div class="params">
			<span class="sm-param-caption"><?php _e( 'Packs:', 'shortcode-mastery' ); ?></span>
			<?php
			foreach ( $packs as $pack ) { ?>
				<a href="<?php echo admin_url('admin.php?page=shortcode-mastery-templates'); ?>&amp;pack=<?php echo $pack['slug']; ?>" <span class="sm-inline-attr sm-inline-param sm-inline-button" data-pack="<?php echo $pack['slug']; ?>"><?php echo esc_html( $pack['name'] ); ?></span></a>
			<?php } ?>
		</div>
		<?php }

	}
	
	/**
     * Render right badge
     */
	
	protected function render_badge_right() {
		
		$groups = $this->get_tax_terms( 'group' );
		
		if ( $groups ) {
				
			foreach ( $groups as $group ) {
				
				switch ( $group['slug'] ) {
					
					case 'premium': echo '<div class="sm-badge premium"></div>'; break;
					case 'featured': echo '<div class="sm-badge featured"></div>'; break;
					default: break;
				}
				
			}
		
		}
		
	}
	
	/**
     * Render left badge
     */
	
	protected function render_badge_left() {
		
		$packs = $this->get_tax_terms( 'pack' );
		
		if ( $packs ) {
				
			echo '<div class="sm-badge-left pack"></div>';
		
		}
		
	}
	
	/**
     * Get terms of taxonomy
     *
     * @param string $tax Taxonomy
     * @return array Terms
     */	
	
	protected function get_tax_terms( $tax = null ) {
		
		if ( isset( $this->data['_embedded'] ) ) {
			
			$embedded = $this->data['_embedded'];
			
			$embedded = (array) $embedded;
			
			if ( isset( $embedded['wp:term'] ) && sizeof( $embedded['wp:term'] ) > 0 ) {
				
				foreach( $embedded['wp:term'] as $taxonomy ) {
					
					
					
					if ( sizeof( $taxonomy ) > 0 ) {
												
						if ( $tax == $taxonomy[0]['taxonomy'] ) return $taxonomy;
						
					}
					
				}
				
			}
		}
		
		return null;
	}

}
?>