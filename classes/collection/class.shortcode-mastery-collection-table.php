<?php
/**
 * Shortcode Mastery Collection Table Class
 *
 * @class   Shortcode_Mastery_Collection_Table
 * @package Shortcode_Mastery
 * @version 2.0.0
 *
 */

class Shortcode_Mastery_Collection_Table extends WP_List_Table {
	
	private $shortcodes;
	
	private $total_items;
	
	private $total_pages;
	
	private $per_page;
	
	private $pack_info;
	
	private $tag_info;
	
	private $group_info;
	
    public function __construct( $shortcodes, $total_items = '', $total_pages = 1, $per_page = 10, $pack_info = '', $tag_info = '', $group_info = '' ) {
	    
	    $this->shortcodes = $shortcodes;
	    
		$this->total_items = $total_items;
		
		$this->total_pages = $total_pages;
		
		$this->per_page = $per_page;
		
		$this->pack_info = $pack_info;
		
		$this->tag_info = $tag_info;
		
		$this->group_info = $group_info;
	    
		parent::__construct( array(
			'singular'=> 'shortcode_mastery_collection_table', //Singular label
			'plural' => 'shortcode_mastery_collection_tables', //plural label, also this well be one of the table css class
			'ajax'   => true //We won't support Ajax for this table
		) );
    }
    
	public function get_columns() {
		return null;
	}
	
	protected function get_table_classes() {
		
		$classes = array( $this->_args['plural'] );
				
	    return $classes;
	}
		
	public function prepare_items() {
				        
        $orderby = (isset($_REQUEST['orderby']) && in_array($_REQUEST['orderby'], array( 'date', 'post_title' ) ) ) ? $_REQUEST['orderby'] : 'post_title';
        
        $order = (isset($_REQUEST['order']) && in_array($_REQUEST['order'], array( 'asc', 'desc') ) ) ? $_REQUEST['order'] : 'asc';
		
        $this->items = $this->shortcodes;
        
        if ( $this->has_items() ) {
        
			$this->set_pagination_args(
			    array(
			        //WE have to calculate the total number of items
			        'total_items'   => $this->total_items,
			        //WE have to determine how many items to show on a page
			        'per_page'  => $this->per_page,
			        //WE have to calculate the total number of pages
			        'total_pages'   => $this->total_pages,
			        // Set ordering values if needed (useful for AJAX)
			        'orderby'   => ! empty( $_REQUEST['orderby'] ) && '' != $_REQUEST['orderby'] ? $_REQUEST['orderby'] : 'title',
			        'order'     => ! empty( $_REQUEST['order'] ) && '' != $_REQUEST['order'] ? $_REQUEST['order'] : 'asc'
			    )
			);
			
		}

	}
	
	public function no_items() {
		
		echo '<p class="sm-no-items">' . __( 'There is no shortcodes in collection yet.', 'shortcode-mastery' ) . '</p>';
		
	}
    
	public function display() {
		
		$tab = 'all';
		
		$tag = '';
		
		$pack = '';
		
		$paged = 1;
		 
		$s = null;
		
		if ( isset( $_REQUEST['tab'] ) ) $tab = sanitize_text_field( $_REQUEST['tab'] );
		
		if ( isset( $_REQUEST['tag'] ) ) $tag = sanitize_text_field( $_REQUEST['tag'] );
		
		if ( isset( $_REQUEST['pack'] ) ) $pack = sanitize_text_field( $_REQUEST['pack'] );
		
		if ( isset( $_REQUEST['paged'] ) ) $paged = absint( $_REQUEST['paged'] );
		
		if ( isset( $_REQUEST['s'] ) ) $s = esc_attr( $_REQUEST['s'] );
		
		?>
		
		<form id="shortcodes-collection-filter" method="get">
			
		<?php
		
		if ( $tab ) echo '<input type="hidden" name="tab" value="' . esc_attr( sanitize_title( $tab ) ) . '">';
		
		if ( $tag ) echo '<input type="hidden" name="tag" value="' . esc_attr( sanitize_title( $tag ) ) . '">';
		
		if ( $pack ) echo '<input type="hidden" name="tag" value="' . esc_attr( sanitize_title( $pack ) ) . '">';
		
		?>
		
		<input type="hidden" name="page" value="shortcode-mastery-templates">
		
		<?php
			
	    wp_nonce_field( 'ajax_shortcode_mastery_collection_table_nonce', 'ajax_shortcode_mastery_collection_table_nonce' );
		
		if ( $this->has_items() ) {
		
		    echo '<input id="order" type="hidden" name="order" value="' . $this->_pagination_args['order'] . '" />';
		    echo '<input id="orderby" type="hidden" name="orderby" value="' . $this->_pagination_args['orderby'] . '" />';
	    
	    }
	    	 
		$singular = $this->_args['singular'];
 
        $this->screen->render_screen_reader_content( 'heading_list' );
        
        echo '<span id="sm-search-description">' . $this->display_searching_desc() . '</span>';
        
        $pack_info = '';
        
        $group_info = '';
        
        if ( $this->pack_info ) $pack_info = $this->pack_info;
        
		echo '<div id="sm-pack-info">' . $pack_info . '</div>';
		
        if ( $this->group_info ) $group_info = $this->group_info;
        
		echo '<div id="sm-group-info">' . $group_info . '</div>';
		
		//if ( ! $this->has_items() ) $this->no_items();
		 
        $this->display_tablenav( 'top' );
		?>
		<div id="the-list" class="sm-collection <?php echo implode( ' ', $this->get_table_classes() ); ?>">						
		<?php $this->display_items_or_placeholder( $tab, $tag, $pack, $paged, $s ); ?>
		</div>
		<?php
        $this->display_tablenav( 'bottom' );
        ?>
		</form>
		<?php
	}
	
	public function display_searching_desc() {
		
		$tag = '';
		
		if ( isset( $_REQUEST['s'] ) || isset( $_REQUEST['tag'] ) ) { 
			
			if ( isset( $_REQUEST['s'] ) ) {
				
				$tag = esc_attr( $_REQUEST['s'] );
				
			} elseif ( isset( $_REQUEST['tag'] ) && $this->tag_info ) $tag = $this->tag_info;
			
			if ( $tag ) return '<p class="sm-collection-desc">' . __( 'Searching results by ', 'shortcode-mastery' ) . '<strong>' . $tag . '</strong>:</p>';
			
		}
		
		return '';
	}
	
    public function display_items_or_placeholder( $tab, $tag, $pack, $paged, $s ) {
	    
        if ( $this->has_items() ) {
	        
            $this->display_items( $tab, $tag, $pack, $paged, $s );
            
        }
    }

    public function display_items( $tab, $tag, $pack, $paged, $s ) {
        
        foreach ( $this->items as $item ) {

			$sm = new Shortcode_Mastery_Collection_Item( $item, $tab, $tag, $pack, $paged, $s );
						
		}
    }

	public function ajax_response() {
		
		if ( $this->items ) {
	 	 	 
	    extract( $this->_args );
	    
		extract( $this->_pagination_args, EXTR_SKIP );
						 
		if ( isset( $_REQUEST['s'] ) ) $s = esc_attr( $_REQUEST['s'] );	    
	    
		if ( isset( $_REQUEST['tab'] ) ) $tab = sanitize_text_field( $_REQUEST['tab'] );
		
		if ( isset( $_REQUEST['tag'] ) ) $tag = sanitize_text_field( $_REQUEST['tag'] );
		
		if ( isset( $_REQUEST['pack'] ) ) $pack = sanitize_text_field( $_REQUEST['pack'] );
		
		if ( isset( $_REQUEST['paged'] ) ) $paged = absint( $_REQUEST['paged'] );
	    
	    ob_start();
	    if ( ! empty( $_REQUEST['no_placeholder'] ) )
	        $this->display_items( $tab, $tag, $pack, $paged, $s );
	    else
	        $this->display_items_or_placeholder( $tab, $tag, $pack, $paged, $s );
	    $rows = ob_get_clean();
	 
	    ob_start(); ?>
		<div class="tablenav <?php echo esc_attr( $which ); ?>">
		 
		        <?php if ( $this->has_items() ): ?>
		        <div class="alignleft actions bulkactions">
		            <?php $this->bulk_actions( $which ); ?>
		        </div>
		        <?php endif;
		        $this->extra_tablenav( $which );
		        $this->pagination( $which );
		?>
		 
		        <br class="clear" />
		    </div>
		<?php
	    $pagination_top = ob_get_clean();
	 
	    ob_start();
	    $this->display_tablenav( 'bottom' );
	    $pagination_bottom = ob_get_clean();
	 
	    $response = array( 'rows' => $rows );
	    $response['packInfo'] = '';
	    $response['groupInfo'] = '';
	    $response['searchDescription'] = $this->display_searching_desc();
	    $response['pagination']['top'] = $pagination_top;
	    $response['pagination']['bottom'] = $pagination_bottom;
	    
	    if ( $this->pack_info ) $response['packInfo'] = $this->pack_info;
	    if ( $this->group_info ) $response['groupInfo'] = $this->group_info;
	    
	    } else {
		   $response = array( 'rows' => $this->no_items() ); 
	    }
	 
	    die( json_encode( $response ) );
	}
	
}