<?php
/**
 * Shortcode Mastery Collection Details Page
 *
 * @class   Shortcode_Mastery_Collection_Details_Page
 * @package Shortcode_Mastery
 * @version 2.0.0
 *
 */

class Shortcode_Mastery_Collection_Details_Page extends Shortcode_Mastery_Collection_Item {
	
	/**
	 * Render Collection Details Page
	 *
	 * @param string $tab Current tab
	 * @param string $tag Current tag
	 * @param string $tag Current pack
	 * @param int $paged Current page
	 * @param string $s Current search term
	 * @param bool $disabled Already added
	 */	
	
	public function render( $tab, $tag, $pack, $paged, $s, $disabled ) {
			
		$style = '';
		
		$source = $this->get_image_source();

		if ( $source ) {
			
			$style = ' style="background-image: url(' . $source . ');background-position: 50% 50%;background-size: cover"';
			
		} 
		?>
		<div class="shortcode-mastery-details-page">
			
			<div class="image-holder"<?php echo $style; ?>>
				<h2>
					<?php echo $this->data['title']['rendered']; ?>
				</h2>
			</div>
			
			<div class="sm-shortcode-details-content">
					<?php echo $this->data['content']['rendered']; ?>
					<?php if ( isset( $this->data['meta']['preview_example'] ) ) echo $this->data['meta']['preview_example'][0]; ?>
			</div>
			
		</div>
		<div class="sm-info-footer">
			<?php if ( current_user_can ( $this->menu_permission ) ) { ?>
			<?php if ( $disabled != 'true' ) { ?>
			<a class="sm-button sm-edit-button sm-item-button-detailed" style="float:right;text-decoration:none" href="<?php echo wp_nonce_url( admin_url('admin.php?page=shortcode-mastery-templates') . '&amp;new_id=' . $this->data['id'] . '&amp;tab=' . $tab . '&amp;tag=' . $tag . '&amp;pack=' . $pack . '&amp;s=' . $s . '&amp;paged=' . $paged, 'add_shortcode_with_id_' . $this->data['id'], 'sm_nonce' ); ?>" title="<?php _e( 'Add shortcode', 'shortcode-mastery' ); ?>" data-id="<?php echo $this->data['id']; ?>"><?php _e( 'Add shortcode', 'shortcode-mastery' ); ?></a>
			<?php } else { ?>
				<a class="sm-button sm-edit-button sm-item-button-detailed-disabled" style="float:right;text-decoration:none;background:#46b450" href="javascript:void(0);" title="<?php _e( 'Shortcode added', 'shortcode-mastery' ); ?>"><?php _e( 'Shortcode added', 'shortcode-mastery' ); ?></a>		
			<?php } ?>
			<?php } ?>
		</div>
	<?php
	}
}
?>