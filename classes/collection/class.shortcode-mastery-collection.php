<?php
/**
 * Shortcode Mastery Collection
 *
 * @class   Shortcode_Mastery_Collection
 * @package Shortcode_Mastery
 * @version 2.0.0
 *
 */

class Shortcode_Mastery_Collection {
	
	/**
	 * Version number
	 *
	 * @access public
	 * @var float
	 */
	
	public $version;
	
	/**
	 * Rest api route
	 *
	 * @access private
	 * @var string
	 */
	
	private $rest_url = 'wp-json/wp/v2/';
	
	/**
	 * Custom rest api route
	 *
	 * @access private
	 * @var string
	 */
	
	private $custom_rest_url = 'wp-json/shortcode/v2/';
	
	/**
	 * Server url
	 *
	 * @access private
	 * @var string
	 */
	
	private $server_url = 'http://app.uncleserj.com/';
	
	/**
	 * Post per page to display in collection
	 *
	 * @access private
	 * @var int
	 */
	
	private $per_page = 12;
	
	/**
	 * Current request headers
	 *
	 * @access private
	 * @var array
	 */
	
	private $headers;
	
	/**
	 * Custom scripts for preview
	 * @access private
	 * @var array	 
	 */
	
	private $custom_embed_scripts;
	
	/**
	 * Custom styles for preview
	 *
	 * @access private
	 * @var array
	 */
	
	private $custom_embed_styles;
	
	/**
	 * Custom embed scripts for preview
	 * @access private
	 * @var array	 
	 */
	
	private $custom_scripts;
	
	/**
	 * Custom embed styles for preview
	 *
	 * @access private
	 * @var array
	 */
	
	private $custom_styles;
		
	/**
	 * Constructor
	 */	

	public function __construct( $version ) {
		
		$this->version = $version;
						
		$this->hooks();
		
	}

	/**
	 * Wordpress Hooks
	 */
	
	public function hooks() {
		
		/**
		 * Ajax Logic
		 */
		
		add_action( 'wp_ajax_ajax_sm_add_shortcode', array( $this, 'ajax_sm_add_shortcode' ) );
		
		add_action( 'wp_ajax_ajax_sm_shortcode_mastery_collection_search', array( $this, 'ajax_shortcode_mastery_collection_search' ) );
		
		add_action( 'wp_ajax_ajax_sm_shortcode_mastery_collection_table', array( $this, 'ajax_shortcode_mastery_collection_table' ) );
		
		/**
		 * Render details page
		 */
		
		add_action( 'admin_action_shortcode-mastery-details-page', array( $this, 'render_details_page' ) );

	}
	
	/**
	 * Ajax Table Pagination
	 */

	public function ajax_shortcode_mastery_collection_table() {
		
		check_admin_referer( 'ajax_shortcode_mastery_collection_table_nonce', 'ajax_shortcode_mastery_collection_table_nonce' );
		
		if ( isset( $_REQUEST ) ) {
			
			$params = array();
			
			$params['per_page'] = $this->per_page;
					
			$params['order'] = 'desc';
					
			$params['orderby'] = 'date';
			
			$params['_embed'] = 'true';
			
			$params = array_merge( $params, $this->get_params() );
					
			$shortcodes = $this->request( 'shortcode-mastery/', $params );
											
			$this->render_form( $shortcodes, true );
		}
			
		wp_die();
	 
	}
	
	/**
	 * Ajax Search
	 */
	
	public function ajax_shortcode_mastery_collection_search() {
				
		check_admin_referer( 'ajax_shortcode_mastery_collection_search_nonce', 'ajax_shortcode_mastery_collection_search_nonce' );
				
		if ( isset( $_REQUEST ) ) {
			
			$params = array();
			
			$params['per_page'] = $this->per_page;
					
			$params['order'] = 'desc';
					
			$params['orderby'] = 'date';
			
			$params['_embed'] = 'true';
			
			$params = array_merge( $params, $this->get_params() );
			
			$params['tab'] = 'search';
			
			$shortcodes = $this->request( 'shortcode-mastery/', $params );
						
			$this->render_form( $shortcodes, true );	
			
		}
				
		wp_die();
	}
	
	/**
	 * Ajax Add Shortcode From Collection
	 */
	
	public function ajax_sm_add_shortcode() {
				
		check_admin_referer( 'shortcode-mastery-ajax', 'security' );
		
		$id = null;
				
		if ( isset( $_POST['value'] ) ) {
									
			$id = $this->add_shortcode( absint( $_POST['value'] ), true );
						
		}
		
		if ( ! intval( $id ) ) {
			
			$values = array();
			
			$values['error'] = $id;
							
			echo json_encode( $values );
			
		} else {
		
			echo $id;	
		}
				
		wp_die();
	}
	
	/**
	 * Render details custom page scripts
	 */
	
	public function render_details_custom_scripts() {
		
		wp_enqueue_style( 'shortcode-mastery-admin', SHORTCODE_MASTERY_URL . 'css/shortcode-mastery-admin.css', array( 'wp-admin' ), $this->version, 'all' );
		
		wp_enqueue_script( 'shortcode-mastery-collection-imagesloaded', SHORTCODE_MASTERY_URL . 'js/imagesloaded.pkgd.min.js', array( 'jquery' ), NULL, true );
		
		wp_enqueue_script( 'shortcode-mastery-collection-ajax', SHORTCODE_MASTERY_URL . 'js/shortcode-mastery-collection.min.js', array( 'jquery' ), $this->version, true );
		
		$vars = array(
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
			'ajaxnonce' => wp_create_nonce( 'shortcode-mastery-ajax' ),
			'added' => __( 'Added', 'shortcode-mastery' ),
			'optionsSaving' => __( 'Shortcode saving...', 'shortcode-mastery' ),
			'shortcodeCreating' => __( 'Shortcode creating...', 'shortcode-mastery' ),
			'successMessage' =>  __( 'Shortcode saved', 'shortcode-mastery' ),
			'successMessageAdded' =>  __( 'Shortcode added', 'shortcode-mastery' ),
			'errorMessage' => __( 'Error in system', 'shortcode-mastery' ),
			'iframe' => ''
		);
		
		if ( defined( 'IFRAME_REQUEST' ) ) {
			
			$vars['iframe'] = true;
		}
				
		wp_localize_script( 'shortcode-mastery-collection-ajax', 'smajax', $vars );
		
		if ( $this->custom_embed_scripts && is_array( $this->custom_embed_scripts ) ) {
						
			foreach ( $this->custom_embed_scripts as $script ) {		
				
				if ( ! wp_script_is( sanitize_title( $script['name'] ), 'registered' ) ) {
																		
					wp_enqueue_script( sanitize_title( $script['name'] ), esc_url( $script['value'] ), array( 'jquery' ), $this->version, true ); 
				}
															
			}
						
		}
		
		if ( $this->custom_embed_styles && is_array( $this->custom_embed_styles ) ) {
			
			foreach ( $this->custom_embed_styles as $style ) {
								
				if ( ! wp_style_is( sanitize_title( $style['name'] ), 'registered' ) ) {
																										
					wp_enqueue_style( sanitize_title( $style['name'] ), esc_url( $style['value'] ) ); 
				
				}
															
			}
			
		}
		
		if ( $this->custom_styles && is_array( $this->custom_styles ) ) {
			
			require_once( SHORTCODE_MASTERY_DIR . 'classes/class.shortcode-mastery-scripts.php' );
										
			new Shortcode_Mastery_Scripts( $this->custom_styles['name'], $this->custom_styles['value'], true, true, false );		
																					
		}
		
		if ( $this->custom_scripts && is_array( $this->custom_scripts ) ) {
			
			require_once( SHORTCODE_MASTERY_DIR . 'classes/class.shortcode-mastery-scripts.php' );
										
			new Shortcode_Mastery_Scripts( $this->custom_scripts['name'], $this->custom_scripts['value'], false, true );	
																						
		}
		
	}
	
	/**
	 * Render embed styles and scripts
	 */	
	
	private function render_embed_styles_scripts( $meta ) {
				
		$a = array();
				
		if ( isset( $meta['embed_styles'] ) ) {
						
			$params = Shortcode_Mastery::decodeString( $meta['params'][0] );

			if ( $params && is_array( $params ) ) {
			
				foreach( $params as $param ) {
				
					$value = '';
					
					if ( isset( $param['value'] ) ) $value = $param['value'];
					
					$a[$param['name']] = $value;
				
				}
			
			}
			
		}
		
		isset( $meta['embed_styles'] ) ? $embed_styles = Shortcode_Mastery::decodeString( $meta['embed_styles'][0] ) : $embed_styles = null;
		
		isset( $meta['embed_scripts'] ) ? $embed_scripts = Shortcode_Mastery::decodeString( $meta['embed_scripts'][0] ) : $embed_scripts = null;
				
		if ( $embed_styles ) {
			
			foreach ( $embed_styles as $k => $style ) {
				
				$embed_styles[ $k ]['name'] = Shortcode_Mastery::getTwig()->render_inline_content( $style['name'], $a );
										
				$embed_styles[ $k ]['value'] = Shortcode_Mastery::getTwig()->render_inline_content( $style['value'], $a );
			
			}
			
			$this->custom_embed_styles = $embed_styles;
			
		}
		
		if ( $embed_scripts ) {
			
			foreach ( $embed_scripts as $k => $script ) {
				
				$embed_scripts[ $k ]['name'] = Shortcode_Mastery::getTwig()->render_inline_content( $script['name'], $a );
			
				$embed_scripts[ $k ]['value'] = Shortcode_Mastery::getTwig()->render_inline_content( $script['value'], $a );
			
			}
			
			$this->custom_embed_scripts = $embed_scripts;
			
		}
		
	}
	
	/**
	 * Render styles and scripts
	 */	
	
	private function render_styles_scripts( $name, $meta ) {
		
		$a = array();
				
		if ( isset( $meta['embed_styles'] ) ) {
						
			$params = Shortcode_Mastery::decodeString( $meta['params'][0] );

			if ( $params && is_array( $params ) ) {
			
				foreach( $params as $param ) {
				
					$value = '';
					
					if ( isset( $param['value'] ) ) $value = $param['value'];
					
					$a[$param['name']] = $value;
				
				}
			
			}
			
		}
		
		isset( $meta['styles'] ) ? $styles = array( 'name' => $name, 'value' => $meta['styles'][0] ) : $styles = null;
		
		isset( $meta['scripts'] ) ? $scripts = array( 'name' => $name, 'value' => $meta['scripts'][0] ) : $scripts = null;
		
		if ( $styles ) {
			
			foreach ( $styles as $k => $style ) {
										
				$styles[$k] = Shortcode_Mastery::getTwig()->render_inline_content( str_replace( "\'", "'", $style ), $a );
			
			}
			
			$this->custom_styles = $styles;
			
		}
		
		if ( $scripts ) {
			
			foreach ( $scripts as $k => $script ) {
			
				$scripts[$k] = Shortcode_Mastery::getTwig()->render_inline_content( str_replace( "\'", "'", $script ), $a );
			
			}
			
			$this->custom_scripts = $scripts;
			
		}
				
	}
	
	/**
	 * Render Details Page
	 */
	
	public function render_details_page() {
								
		if ( ! defined( 'IFRAME_REQUEST' ) && 
			 absint( $_REQUEST[ 'new_id' ] ) && 
		     isset( $_REQUEST[ 'sm_nonce' ] ) && 
		     wp_verify_nonce( $_REQUEST[ 'sm_nonce' ], 'shortcode-mastery-details-page-' . absint( $_REQUEST[ 'new_id' ] ) ) 
		    ) {
			
			$disabled = false;
			
			isset( $_REQUEST[ 'tab' ] ) ? $tab = sanitize_title( $_REQUEST[ 'tab' ] ) : $tab = 'all';
			
			isset( $_REQUEST[ 'tag' ] ) ? $tag = sanitize_title( $_REQUEST[ 'tag' ] ) : $tag = '';
			
			isset( $_REQUEST[ 'pack' ] ) ? $pack = sanitize_title( $_REQUEST[ 'pack' ] ) : $pack = '';
			
			isset( $_REQUEST[ 'paged' ] ) ? $paged = absint( $_REQUEST[ 'paged' ] ) : $paged = 1;
			
			isset( $_REQUEST[ 's' ] ) ? $s = esc_attr( $_REQUEST[ 's' ] ) : $s = '';
			
			if ( isset( $_REQUEST[ 'disabled' ] ) && $_REQUEST[ 'disabled' ] == 'true' ) $disabled = 'true';
				    
		    define( 'IFRAME_REQUEST', true );
		    		    	    		
			$params = array();
										
			$params['_embed'] = 'true';
			
			$data = $this->request( 'shortcode-mastery/' . absint( $_REQUEST[ 'new_id' ] ), $params );
			
			if ( $data ) {
								
				$meta = $this->request( 'meta/' , array( 'id' => absint( $_REQUEST[ 'new_id' ] ) ), true );
				
				if ( $meta ) {
								
					$this->render_embed_styles_scripts( $meta );
					
					$this->render_styles_scripts( $data['slug'], $meta );
					
					if ( isset( $meta['preview_example'] ) ) $data['meta']['preview_example'] = $meta['preview_example'];
					
					add_action( 'admin_enqueue_scripts', array( $this, 'render_details_custom_scripts' ) );
					
				}
																				
				iframe_header();
				 			
				$page = new Shortcode_Mastery_Collection_Details_Page( $data, $tag, $tag, $pack, $paged, $s, $disabled );
								
				iframe_footer();
							
			}
	    
	    }
	    
	    exit;
	}
	
	/**
     * Generate url-friendly parameters
     *
     * @param array $params Parameters array
     * @return string Parameters string
     */
	
	public function get_params_string( $params = null ) {
		
		$params_string = '';
		
		if ( $params && is_array( $params ) ) {
			
			$params_string = '?';
			
			$count = 0;
			
			foreach ( $params as $key=>$value ) {
				
				if ( $count != 0 ) $params_string .= '&';
				
				$params_string .= $key . '=' . $value;
				
				$count++;
				
			}
			
		}
		
		return $params_string;
		
	}
	
	/**
     * Perform request
     *
     * @param string $route Route
     * @return array $params Parameters array
     */
	
	public function request( $route, $params = null, $custom_rest = false ) {
		
		$response = null;
		
		$params_string = $this->get_params_string( $params );
		
		$rest_url = $this->rest_url;
		
		if ( $custom_rest ) $rest_url = $this->custom_rest_url;
				
		$request = wp_remote_get( $this->server_url . $rest_url . $route . $params_string );
					 
		if ( ! is_wp_error( $request ) && wp_remote_retrieve_response_code( $request ) == '200' ) {
							
			$this->headers = wp_remote_retrieve_headers( $request );
			
			$body = wp_remote_retrieve_body( $request );
			
			$response = json_decode( $body, true );
			
			if ( $response ) return $response;
			
		} elseif ( is_wp_error( $request ) ) {
			
			$error_string = $request->get_error_message();
			
			$this->admin_notice__error( $error_string );
		
		}
	 
	    return $response;
		
	}
	
	/**
     * Get group description
     *
     * @param string $pack Pack slug
     * @return string HTML Pack name and description
     */
	
	private function get_group_info( $group ) {
		
		$output = '';
				
		$group_desc = null;

		$group = $this->request( 'tax/' , array( 'slug' => $group, 'tax' => 'group' ), true );
				
		if ( $group ) {
				
			$group_desc = $group['description'];
			
			$output .= '<div class="sm-pack-desc">' . wpautop($group_desc) . '</div>';
					
		}
		
		return $output;
				
	}
	
	/**
     * Get pack name and description
     *
     * @param string $pack Pack slug
     * @return string HTML Pack name and description
     */
	
	private function get_pack_info( $pack ) {
		
		$output = '';
		
		$pack_name = null;
		
		$pack_desc = null;

		$pack = $this->request( 'tax/' , array( 'slug' => $pack, 'tax' => 'pack' ), true );
		
		if ( $pack ) {
							
			$pack_name = $pack['name'];
			
			$output .= '<p class="sm-pack-title">' . $pack_name . '</p>';
				
			$pack_desc = $pack['description'];
			
			$output .= '<div class="sm-pack-desc">' . wpautop($pack_desc) . '</div>';
					
		}
		
		return $output;
				
	}
	
	/**
     * Get tag name
     *
     * @param string $tag Tag slug
     * @return string Tag name
     */
	
	private function get_tag_info( $tag ) {
		
		$output = '';
		
		$tag_name = null;

		$tag = $this->request( 'tax/' , array( 'slug' => $tag, 'tax' => 'subtag' ), true );
		
		if ( $tag ) {
							
			$tag_name = $tag['name'];
			
			$output .= $tag_name;
					
		}
		
		return $output;
				
	}
	
	/**
     * Render megapacks
     */
	
	private function render_packs( $packs = null ) { 
						
		if ( $packs ) { ?>
						
		<ul class="sm-packs">
			
			<strong style="padding-right:10px"><?php _e( 'Featured packs', 'shortcode-mastery' ); ?>:</strong>

			<?php foreach ( $packs as $pack ) { 
				
				if ( $pack['count'] > 0 ) {
			?>
			<li>
				<a href="<?php echo admin_url('admin.php?page=shortcode-mastery-templates'); ?>&amp;pack=<?php echo $pack['slug']; ?>" class="sm-button sm-loop-button" data-pack="<?php echo $pack['slug']; ?>"><?php _e( $pack['name'], 'shortcode-mastery' ); ?></a>
			</li>
			<?php } } ?>
		</ul>
		
	<?php }
	}
	
	/**
     * Render useful tags
     *
     * @param string $current_tab Current active tab
     */
	
	private function render_tags( $tags = null, $current_tab = 'all' ) { 
						
		if ( $tags ) { ?>
						
		<ul class="sm-tags">
			
			<strong style="padding-right:10px"><?php _e( 'Related tags', 'shortcode-mastery' ); ?>:</strong>

			<?php foreach ( $tags as $tag ) { 
				
				if ( $tag['count'] > 0 ) {
			?>
			<li>
				<a href="<?php echo admin_url('admin.php?page=shortcode-mastery-templates'); ?>&amp;tag=<?php echo $tag['slug']; ?>&amp;tab=<?php echo $current_tab; ?>" class="sm-button sm-edit-button" data-tag="<?php echo $tag['slug']; ?>" data-tab="<?php echo $current_tab; ?>"><?php echo $tag['name']; ?></a>
			</li>
			<?php } } ?>
		</ul>
		
	<?php }
	}
	
	/**
     * Render groups menu
     */
     
	private function render_menu( $groups = null ) {
		
		$search = $examples = '';
		
		$tabs = array();
		
		$tabs['all'] = '';
		$tabs['search'] = '';
				
		if ( $groups ) {
		
		foreach ( $groups as $group ) {
			
			$tabs[$group['slug']] = '';
			
			if ( isset( $_GET['tab'] ) && $_GET['tab'] == $group['slug'] ) {
							
				$tabs[$group['slug']] = 'current';
				
			}
			
		}
		
		}
		
		if ( isset( $_GET['tab'] ) && $_GET['tab'] == 'search' ) $tabs['search'] = 'current';
		
		if ( ! array_search( 'current', $tabs ) ) $tabs['all'] = 'current';
							
		?>
		<div class="wp-filter sm-filter">
			<ul class="filter-links sm-filter-links">
				<?php if ( isset( $_REQUEST['s'] ) && $_REQUEST['s'] !== null ) { $search = esc_attr( $_REQUEST['s'] ); ?>
				<li>
					<a href="<?php echo admin_url('admin.php?page=shortcode-mastery-templates'); ?>&amp;tab=search&amp;s=<?php echo $search; ?>" class="<?php echo $tabs['search'] ?>" data-tab="search"><?php _e( 'Search Results', 'shortcode-mastery' ); ?></a>
				</li>
				<?php } ?>
				<li>
					<a href="<?php echo admin_url('admin.php?page=shortcode-mastery-templates'); ?>&amp;tab=all" class="<?php echo $tabs['all']; ?>" data-tab="all"><?php _e( 'All Shortcodes', 'shortcode-mastery' ); ?></a>
				</li>
				<?php if ( $groups ) {
					
				foreach ( $groups as $group ) { 
					
					if ( $group['count'] > 0 ) {
						
					if ( $group['slug'] != 'examples' ) {
				?>
				<li>
					<a href="<?php echo admin_url('admin.php?page=shortcode-mastery-templates'); ?>&amp;tab=<?php echo $group['slug']; ?>" class="<?php echo $tabs[$group['slug']]; ?>" data-tab="<?php echo $group['slug']; ?>">
						<?php echo $group['name']; ?>
					</a>
				</li>
				<?php } else {
				ob_start(); ?>
				<li>
					<a href="<?php echo admin_url('admin.php?page=shortcode-mastery-templates'); ?>&amp;tab=<?php echo $group['slug']; ?>" class="<?php echo $tabs[$group['slug']]; ?>" data-tab="<?php echo $group['slug']; ?>">
						<?php echo $group['name']; ?>
					</a>
				</li>
				<?php
				$examples = ob_get_clean();
				
				}
				
				} } } ?>
				<?php echo $examples; ?>
			</ul>
		
			<form class="search-form search-shortcodes" method="get">
				<input type="hidden" name="tab" value="search">
				<input type="hidden" name="page" value="shortcode-mastery-templates">
				<?php wp_nonce_field( 'ajax_shortcode_mastery_collection_search_nonce', 'ajax_shortcode_mastery_collection_search_nonce' ); ?>
				<label>
					<span class="screen-reader-text"><?php _e( 'Search Shortcodes', 'shortcode-mastery' ); ?></span>
					<input type="search" name="s" value="<?php echo esc_attr( $search ); ?>" class="wp-filter-search sm-filter-search" placeholder="<?php _e( 'Search shortcodes...', 'shortcode-mastery' ); ?>">
				</label>
				<input type="submit" id="search-submit" class="button hide-if-js" value="<?php _e( 'Search Shortcodes', 'shortcode-mastery' ); ?>" aria-describedby="live-search-desc">	
			</form>
		</div> <?php
	}
	
	/**
     * Render top bar
     */
     
	private function render_top_bar() {
		
		//$all = $this->request( 'taxonomies/', null, true );
		
		$all = array();
		
		$all['group'] = array(
			array( 'name' => 'Featured', 'slug' => 'featured', 'count' => 1 ),
			array( 'name' => 'Examples', 'slug' => 'examples', 'count' => 1 ),
		);
		
		if ( $all ) {
			
			if ( isset( $all['group'] ) ) $this->render_menu( $all['group'] );
			
			if ( isset( $_GET['tab'] ) && $_GET['tab'] == 'examples' ) {
				
			} else {
			
				if ( isset( $all['subtag'] ) ) $this->render_tags( $all['subtag'] );
			
				if ( isset( $all['pack'] ) ) $this->render_packs( $all['pack'] );
			
			}
		}
		
	}
	
	/**
     * Render main form
     */
	
	private function render_form( $shortcodes, $ajax = false ) {
		
		$pack_info = '';
		
		$tag_info = '';
		
		$group_info = '';
		
		$total_items = '';
		
		$total_pages = 1;
		
		if ( $this->headers && isset( $this->headers['X-WP-Total'] ) && isset( $this->headers['X-WP-TotalPages'] ) ) {
			
			$total_items = $this->headers['X-WP-Total'];
				
			$total_pages = $this->headers['X-WP-TotalPages'];
			
		}
		
		if ( $ajax ) {

			if ( isset( $_REQUEST['pack'] ) ) $pack_info = $this->get_pack_info( sanitize_title( $_REQUEST['pack'] ) );
			
			if ( isset( $_REQUEST['tag'] ) ) $tag_info = $this->get_tag_info( sanitize_title( $_REQUEST['tag'] ) );
			
			$tab = 'all';
			
			if ( isset( $_REQUEST['tab'] ) ) $tab = sanitize_title( $_REQUEST['tab'] );
			
			if ( $tab != 'all' ) $group_info = $this->get_group_info( $tab );
					
			$table = new Shortcode_Mastery_Collection_Table( $shortcodes, $total_items, $total_pages, $this->per_page, $pack_info, $tag_info, $group_info );
			
			$table->prepare_items();
			
			$table->ajax_response();
			
		} else {
			
			$table = new Shortcode_Mastery_Collection_Table( $shortcodes, $total_items, $total_pages, $this->per_page, $pack_info, $tag_info, $group_info );
			
			$table->prepare_items();
					
			$table->display();
		
		}

	}
	
	/**
     * Get sanitized parameters
     *
     * @return array $params Sanitized parameters
     */
	
	public function get_params() {
		
		$params = array();
		
		foreach ( $_REQUEST as $key => $value ) {
			
			switch ( $key ) {
			
				case 'id': if ( isset( $_REQUEST[ $key ] ) && absint( $_REQUEST[ $key ] ) ) $params[ 'id' ] = absint( $value ); break;
				case 'paged': if ( isset( $_REQUEST[ $key ] ) && absint( $_REQUEST[ $key ] ) ) $params[ 'page' ] = absint( $value ); break;
				case 'tab': if ( isset( $_REQUEST[ $key ] ) && $_REQUEST[ $key ] != 'all' && $_REQUEST[ $key ] != 'search' ) $params[ 'filter[group]' ] = sanitize_title( $value ); 
				if ( $_REQUEST[ $key ] != 'examples' ) $params['group_exclude'] = 18; 
				break;
				case 'tag': if ( isset( $_REQUEST[ $key ] ) ) $params[ 'filter[subtag]' ] = sanitize_title( $value ); break;
				case 'pack': if ( isset( $_REQUEST[ $key ] ) ) $params[ 'filter[pack]' ] = sanitize_title( $value ); break;
				case 's': if ( isset( $_REQUEST[ $key ] ) ) $params[ 'filter[s]' ] = esc_attr( $_REQUEST[ $key ] ); break;
				
				default: break;
			}
			
		}
				
		return $params;
		
	}
	
	/**
     * Render collection
     */

	public function render() {
						
		$s = $tag = $pack = '';
		
		$tab = 'all';
		
		$paged = 1;
		
		$params = array();
		
		$params['per_page'] = $this->per_page;
				
		$params['order'] = 'desc';
				
		$params['orderby'] = 'date';
				
		$params['_embed'] = 'true';
		
		$params = array_merge( $params, $this->get_params() );
		
		if ( ! isset( $params['filter[group]'] ) ) $params['group_exclude'] = 18; 
		
		$this->request_add_shortcode();	
		
		$this->render_top_bar();
		
		//$shortcodes = $this->request( 'shortcode-mastery/', $params );
		
		$shortcodes = null;
		
		echo '<div id="shortcodes-mastery-collection">';
		
		echo '<noscript><p class="sm-no-items">' . __( 'Please enable Javascript to load shortcodes.', 'shortcode-mastery' ) . '</p></noscript>';
					
		$this->render_form( $shortcodes );
		
		echo '</div>';

	}
	
	/**
     * Add shortcode
     */
	
	public function request_add_shortcode() {
		
		if ( isset( $_REQUEST['new_id'] ) && absint( $_REQUEST['new_id'] ) && wp_verify_nonce($_REQUEST['sm_nonce'], 'add_shortcode_with_id_' . $_REQUEST['new_id']) ) {
			
			$id = $this->add_shortcode( absint( $_REQUEST['new_id'] ) );
			
			if ( intval( $id ) ) {
				
					$this->admin_notice__success();
				
			}
			
		}
	}
	
	/**
     * Add shortcode to database
     *
     * @param int $id Remote Shortcode ID
     * @return int $new_id Shortcode ID
     */
     
	private function add_shortcode( $id, $ajax = false ) {
				
		$params = array();
		
		$new_id = null;
						
		$params['_embed'] = 'true';
		
		$shortcode = $this->request( 'shortcode-mastery/' . $id, $params );
		
		if ( $shortcode ) {
			
			$shortcode = (array) $shortcode;
			
			$sm = array();
			
			$meta = $shortcode['meta'];
					
			$sm['shortcode_title'] = sanitize_text_field( $shortcode['title']['rendered'] );
			
			$sm['shortcode_content'] = wp_kses_post( $shortcode['content']['rendered'] );
			
			$sm['shortcode_excerpt'] = wp_kses_post( $shortcode['excerpt']['rendered'] );
			
			foreach( $shortcode['meta'] as $key => $value ) {
				
				$sm[$key] = $value;
				
			}
			
			$sm['not_editable'] = 1;
			
			$sm['icon_source'] = '';
						
			$sm['thumbnail_source'] = $this->get_image_source( $shortcode );
			
			$sm['icon_source'] = $this->get_icon_source( $shortcode );
						
			$new_id = Shortcode_Mastery::addShortcode( $sm, $ajax );
			
		}
		
		return $new_id;
		
	}
	
	/**
     * Get image source from request
     *
     * @param array $data Shortcode data
     * @return string Image Source (Large or Full size)
     */
		
	public function get_image_source( $data ) {
		
		$source = null;
		
		$image_name = $data['slug'] . '-thumbnail';
						
		if ( isset( $data['_embedded'] ) ) {
			
			$embedded = $data['_embedded'];
			
			$embedded = (array) $embedded;
			
			if ( isset($embedded['wp:featuredmedia']) && sizeof( $embedded['wp:featuredmedia'] ) > 0 ) {
			
				$sizes = $embedded['wp:featuredmedia'][0]['media_details']['sizes'];
			
				if ( isset( $sizes['shortcode'] ) ) {
					
					$source = $sizes['shortcode']['source_url'];
					
				} else {
					
					$source = $sizes['full']['source_url'];
				}
			
			}
												
		}
		
		if ( $source ) {
			
			$source = $this->download_media( $source, $image_name );
			
			if ( is_wp_error( $source ) ) $source = null;
			
		}
		
		return $source;
	}
	
	/**
     * Get icon source from request
     *
     * @param array $data Shortcode data
     * @return string Icon Source
     */
	
	public function get_icon_source( $data ) {
		
		$source = null;
		
		$image_name = $data['slug'] . '-icon';
						
		if ( isset( $data['meta']['icon_source'] ) ) {
			
			$id = $data['meta']['icon_source'];
			
			$src = $this->request( 'attachment/', array( 'id' => $id ), true );
			
			if ( $src ) $source = $src[0]; 
												
		}
		
		if ( $source ) {
			
			$source = $this->download_media( $source, $image_name );
			
			if ( is_wp_error( $source ) ) $source = null;
			
		}
		
		return $source;		
	}
	
	public function download_media( $url = '', $image_name = null, $post_id = false  ) {
	   
		require_once( ABSPATH . 'wp-admin/includes/file.php' );
		
		$timeout_seconds = 1;
		
		$temp_file = download_url( $url, $timeout_seconds );
		
		$ext = pathinfo( $url, PATHINFO_EXTENSION );
				
		if ( ! $image_name ) $image_name = basename( $url );
		
		if ( !is_wp_error( $temp_file ) ) {
		
			// Array based on $_FILE as seen in PHP file uploads
			$file = array(
				'name'     => $image_name . '.' . $ext,
				'type'     => mime_content_type( $temp_file ),
				'tmp_name' => $temp_file,
				'error'    => 0,
				'size'     => filesize( $temp_file ),
			);
		
			$overrides = array(
				// Tells WordPress to not look for the POST form
				// fields that would normally be present as
				// we downloaded the file from a remote server, so there
				// will be no form fields
				// Default is true
				'test_form' => false,
		
				// Setting this to false lets WordPress allow empty files, not recommended
				// Default is true
				'test_size' => true,
			);
		
			// Move the temporary file into the uploads directory
			
			$results = wp_handle_sideload( $file, $overrides );
					
			if ( !empty( $results['error'] ) ) {
				
				return new WP_Error( 'no-image', __( 'Broken Image', 'shortcode-mastery' ) );
				
			} else {
		
				$filename  = $results['file']; // Full path to the file
				$local_url = $results['url'];  // URL to the file in the uploads dir
				$type      = $results['type']; // MIME type of the file
		
				return $results['url'];
			}
		
		}
		
	}
	
	/**
     * Admin Success Notice
     */
	
	public function admin_notice__success() {
	    ?>
	    <div class="notice notice-success is-dismissible">
	        <p><?php _e( 'Shortcode was added!', 'shortcode-mastery' ); ?></p>
	    </div>
	    <?php
	}
	
	/**
     * Admin Error Notice
     */
	
	public function admin_notice__error( $message = '' ) {
		
		if ( $message == '' ) $message = __( 'Error in system!', 'shortcode-mastery' );
	    ?>
	    <div class="notice notice-error is-dismissible">
	        <p><?php echo $message; ?></p>
	    </div>
	    <?php
	}
	
	/**
	 * Dump Helper
	 */
	
	private function dump( $value ) {
		
		echo '<pre>';
		var_dump( $value );
		echo '</pre>';
		
	}

}
?>