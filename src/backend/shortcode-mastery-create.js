/**
 * Shortcode Mastery Create Scripts
 *
 * Version: 1.0.2
 * Requires: jQuery v1.7+
 *
 */

var $ = jQuery.noConflict();

$(document).ready(function() {

	// Ajax Loader
	$('.sm-submit-shortcode').after('<div id="sm-ajax-loader-wrapper">' + '<div id="smLoaderObject">' + '<div class="loader-inner line-scale-pulse-out-rapid">' + '<div></div>' + '<div></div>' + '<div></div>' + '<div></div>' + '<div></div>' + '</div>' + '</div>' + '<span id="smSuccessMessage"></span>' + '<span id="smErrorMessage"></span>' + '</div>');

	// Ajax Submit Options
	$('#shortcode-mastery-form').submit(function() {
		$('.notice').remove();
		$('#wp-admin-bar-sm-submit-top-button').find('div').html('<img class="sm-admin-bar-icon" src="'+ smajax.iconUrl + '">' + smajax.optionsSaving);
		$('#smSuccessMessage, #smErrorMessage').hide();
		$('#smLoaderObject').fadeIn(300);
		$.ajax({
			url: smajax.ajaxurl,
			type: 'POST',
			cache: true,
			timeout: 8000,
			data: {
				action: 'ajax_sm_submit',
				security: smajax.ajaxnonce,
				value: $(this).serialize()
			},
			error: function() {
				$('#wp-admin-bar-sm-submit-top-button').find('div').html('<img class="sm-admin-bar-icon" src="'+ smajax.iconUrl + '">' + smajax.errorMessage);
				setTimeout("jQuery('#wp-admin-bar-sm-submit-top-button').find('div').fadeOut(300, function() { jQuery(this).text(''); jQuery(this).show(); });", 2800);
				$('#smLoaderObject').fadeOut(300, function() {
					$('#smErrorMessage').html(smajax.errorMessage);
					$('#smErrorMessage').fadeIn(300);
					setTimeout("jQuery('#smErrorMessage').fadeOut('slow');", 2000);
				});
				
			},
			success: function(returnedData) {
			    try {
					var resp = JSON.parse(returnedData);
					
					if ( ! resp.error ) {
						
						$('#sm-shortcode-title').val(resp.title);
						$('#sm-shortcode-name').text(resp.code);
						$('#wp-admin-bar-sm-submit-top-button').find('div').html('<img class="sm-admin-bar-icon" src="'+ smajax.iconUrl + '">' + smajax.successMessage);
						setTimeout("jQuery('#wp-admin-bar-sm-submit-top-button').find('div').fadeOut(300, function() { jQuery(this).text(''); jQuery(this).show(); });", 2800);
						$('#smLoaderObject').fadeOut(300, function() {
							$('#smSuccessMessage').html(smajax.successMessage);
							$('#smSuccessMessage').fadeIn(300);
							setTimeout("jQuery('#smSuccessMessage').fadeOut('slow');", 2000);
						});
					
					} else {
						
						$("html, body").animate({ scrollTop: 0 });
						$('.shortcode-mastery-nav').prepend(resp.error);
						
						$('#wp-admin-bar-sm-submit-top-button').find('div').html('<img class="sm-admin-bar-icon" src="'+ smajax.iconUrl + '">' + smajax.errorMessage);
						setTimeout("jQuery('#wp-admin-bar-sm-submit-top-button').find('div').fadeOut(300, function() { jQuery(this).text(''); jQuery(this).show(); });", 2800);
						$('#smLoaderObject').fadeOut(300, function() {
							$('#smErrorMessage').html(smajax.errorMessage);
							$('#smErrorMessage').fadeIn(300);
							setTimeout("jQuery('#smErrorMessage').fadeOut('slow');", 2000);
						});						
					}
			    } catch(e) {
				    window.location = returnedData;
			    }
			}
		});
		return false;
	});
	
	bind_remove_button();

	var file_frame;
	var wp_media_post_id = wp.media.model.settings.post.id; // Store the old id
	var set_to_post_id = 44;
	var removeButton = '<a class="sm-remove-icon" title="' + smajax.removeButton + '" href="javascript:void(0);"><i class="icon-cancel"></i></a>';
	
	$('#upload_icon_button').on('click', function( event ){
		event.preventDefault();

		// If the media frame already exists, reopen it.
		if ( file_frame ) {
			// Set the post ID to what we want
			file_frame.uploader.uploader.param( 'post_id', set_to_post_id );
			// Open frame
			file_frame.open();
			return;
		} else {
			// Set the wp.media post id so the uploader grabs the ID we want when initialised
			wp.media.model.settings.post.id = set_to_post_id;
		}
		// Create the media frame.
		file_frame = wp.media.frames.file_frame = wp.media({
			title: smajax.uploaderTitle,
			button: {
				text: smajax.useTitle,
			},
			multiple: false	// Set to true to allow multiple files to be selected
		});
				
		// When an image is selected, run a callback.
		file_frame.on( 'select', function() {
			// We set multiple to false so only get one image from the uploader
			attachment = file_frame.state().get('selection').first().toJSON();
			// Do something with attachment.id and/or attachment.url here
			$( '#sm-icon-url' ).val( attachment.url + '|' + attachment.id );
			$( '.sm-def-icon' ).attr( 'src', attachment.url );
			$( '.sm-def-icon' ).attr( 'srcset', attachment.url + ', ' + attachment.url + ' 2x' );
			
			$( '.sm-icon-title-holder' ).prepend(removeButton);
			
			bind_remove_button();
			
			// Restore the main post ID
			wp.media.model.settings.post.id = wp_media_post_id;
		});
			// Finally, open the modal
			file_frame.open();
	});
	// Restore the main ID when the add media button is pressed
	$( 'a.add_media' ).on( 'click', function() {
		wp.media.model.settings.post.id = wp_media_post_id;
	});

});

function bind_remove_button() {

	$('.sm-remove-icon').on('click', function( e ) {
		e.preventDefault();
		$( '.sm-remove-icon').remove();
		$( '#sm-icon-url' ).val( '' );
		$( '.sm-def-icon' ).attr( 'src', smajax.defUrl );
		$( '.sm-def-icon' ).attr( 'srcset', smajax.defUrl + ', ' + smajax.defUrl2x + ' 2x' );
	});
	
}