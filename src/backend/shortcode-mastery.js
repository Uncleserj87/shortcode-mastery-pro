//import Vue from 'vue';
import bus from './components/Bus.js';

import smDic from './components/Dictionary.vue';
import smTagButton from './components/TagButton.vue';
import smEditor from './components/AceEditor.vue';
import smMethodButton from './components/MethodButton.vue';
import smQueryBuilder from './components/QueryBuilder.vue';
import smCheckbox from './components/Checkbox.vue';
import { basePort } from 'portfinder';

Vue.component('sm-query-builder', smQueryBuilder);
Vue.component('sm-dic', smDic);
Vue.component('sm-editor', smEditor);
Vue.component('sm-method-button', smMethodButton);
Vue.component('sm-tag-button', smTagButton);
Vue.component('sm-checkbox', smCheckbox);

const collapseMixin = {
	data: {
		collapse: 'sm-collapsed'
	},
	methods: {
		onCollapse: function() {
			this.collapse == '' ? this.collapse = 'sm-collapsed' : this.collapse = '';
		}
	},
	computed: {
		sign: function() {
			var sign;
			if (this.collapse == '') {
				sign = '&minus;';
			} else {
				sign = '&plus;';
			}
			return sign;
		}
	}
}
const dicMixin = {
	data: {
		rows: []
	},
	methods: {
		onRows: function(rows) {
			this.rows = rows;
		},
	},
}
const editorMixin = {
	data: {
		editor: null,
	},
	methods: {
		addMethod: function(method) {
			this.editor.insert(method);
			this.editor.focus();
		},
		onInit: function(editor) {
			if (this.editor === null) this.editor = editor;
		}
	}	
}
const sm_title = new Vue({
	el: '#sm-title-block',
	mixins: [collapseMixin],
	data: {
		collapse: '',
		shortcode_codes: '',
		wpbackery: null,
		elementor: null
	},
	methods: {
		onWPBackery: function( value ) {
			this.wpbackery = value;
		},
		onElementor: function( value ) {
			this.elementor = value;
		}
	}
});
const sm_params = new Vue({
	el: '#sm-parameters-block',
	mixins: [dicMixin, collapseMixin],
	data: {
		collapse: '',
		rows: []
	},
	methods: {
		onRows: function(rows) {
			this.rows = rows.sort( function(a,b) { 
				if ( a.hasOwnProperty('checkbox') && a.checkbox ) { return -1 }
				else if ( b.hasOwnProperty('checkbox') && b.checkbox ) { return 1; }
				else { return 0; } });
			this.onChange(rows);
		},
		onChange: function(data) {
			var string = '';
			data.sort( function(a,b) { 
				if ( a.hasOwnProperty('checkbox') && a.checkbox ) { return -1 }
				else if ( b.hasOwnProperty('checkbox') && b.checkbox ) { return 1; }
				else { return 0; } });
			data.filter(function(row) {
				var trueValue = row.value;
				try {
					var val = JSON.parse(row.value);
					if (typeof val == 'object' && Object.keys(val).length) {
						trueValue = '<a href="javascript:void(0);" data-placement="bottom" rel="tooltip" class="sm-info" title="' + row.value.replace(/"/g, '&quot;').replace(/(\])/g, '&#93').replace(/(\[)/g, '&#91;') + '">JSON</a>';
					}
				} catch (e) {}
				string += ' ' + row.name + '="' + trueValue.replace(/"/g, "'").replace(/(\])/g, '}').replace(/(\[)/g, '{') + '"';
			});
			
			sm_title.shortcode_codes = string;
		}
	},
	computed: {
		wpbackery: function() {
			return sm_title.wpbackery ? true : false;
		},
		elementor: function() {
			return sm_title.elementor ? true : false;
		}
	}
});
const sm_query = new Vue({
	el: '#sm-query-block',
	mixins: [dicMixin, collapseMixin],
	data: {
		collapse: "sm-collapsed",
		isNotHidden: false,
		currentGroup: '',
		qb: [],
		paramsRows: sm_params.rows
	},
	methods: {
		listGroups: function(group) {
			this.currentGroup.className = 'sm-hidden args-group';
			var group = document.getElementById(group + '-group');
			group.className = '';
			this.currentGroup = group;
		},
		editAtts: function(row) {
			bus.$emit('editCurrentArg', 'query-builder', row);
		},
		deleteAtts: function(row) {
			bus.$emit('deleteCurrentArg', 'query-builder', row);
		},
		onCheckMethod: function(method) {
			bus.$emit('checkMethod', 'query-dic', method);
		},
		onQueryChange: function(values, method) {
			var rows = [{
				name: method,
				value: values
			}];
			bus.$emit('fullDic', 'query-dic', rows);
		},
		addTemplateArgs: function(rows) {
			bus.$emit('fullDic', 'query-dic', rows);
		}
	}
});
const sm_query_main = new Vue({
	el: '#sm-query-main-block',
	delimiters: ['[[',']]'],
	mixins: [collapseMixin, editorMixin],
	data: {
		collapse: "sm-collapsed",
		main: '',
		rows: sm_params.rows
	},
});
const sm_main = new Vue({
	el: '#sm-main-block',
	delimiters: ['[[',']]'],
	mixins: [collapseMixin, editorMixin],
	data: {
		collapse: "sm-collapsed",
		main_content: '',
		rows: sm_params.rows
	},
});
const sm_scripts = new Vue({
	el: '#sm-scripts-block',
	delimiters: ['[[',']]'],
	mixins: [collapseMixin, dicMixin, editorMixin],
	data: {
		collapse: "sm-collapsed",
		scripts: '',
		paramsRows: sm_params.rows
	},
});
const sm_styles = new Vue({
	el: '#sm-styles-block',
	delimiters: ['[[',']]'],
	mixins: [collapseMixin, dicMixin, editorMixin],
	data: {
		collapse: "sm-collapsed",
		styles: '',
		paramsRows: sm_params.rows
	},
});
const sm_popup = new Vue({
	el: '#sm-popup-wrapper',
	delimiters: ['[[',']]'],
	data: {
		content: null,
		cssClass: false
	},
	mounted: function() {
		var self = this;
		bus.$on( 'popupContent', function( content ) {
			self.content = content;
			self.cssClass = true;
		});
        window.addEventListener('keyup', function(event) {
            if (event.keyCode === 27) {
				self.cssClass = false;
				self.content = null;
            }
        });
	},
	methods: {
		closePopup: function() {
			this.cssClass = false;
			this.content = null;
		}
	}
});