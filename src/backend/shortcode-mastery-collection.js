/**
 * Shortcode Mastery Collection Scripts
 *
 * Version: 1.1.4
 * Requires: jQuery v1.7+
 *
 */
var $ = jQuery.noConflict();

var in_progress = false;

bind_links();
bind_animation();

$(document).ready(function() {
	
	// Ajax Load Shortcodes
	
	if ( smajax.iframe != true ) {
	
		$('#shortcodes-mastery-collection').before('<div id="sm-ajax-loader-wrapper" class="sm-collection-loader">' + '<div id="smLoaderObject">' + '<div class="loader-inner line-scale-pulse-out-rapid">' + '<div></div>' + '<div></div>' + '<div></div>' + '<div></div>' + '<div></div>' + '</div>' + '</div>' + '<span id="smSuccessMessage"></span>' + '<span id="smErrorMessage"></span>' + '</div>');
		
		$('.sm-no-items').remove();
		
		var tab = 'all';
		var paged = 1;
		if ( getAllUrlParams().tab ) tab = getAllUrlParams().tab;
		if ( getAllUrlParams().paged ) paged = getAllUrlParams().paged;
		        
	    var data = {
	        paged: paged,
	        tab: tab,
			tag: getAllUrlParams().tag,
			pack: getAllUrlParams().pack,
			s: getAllUrlParams().s
	    };
	    
	    $('#smLoaderObject').fadeIn(300);
	    $("html, body").animate({ scrollTop: 0 });
	    
	    $.ajax({
	        // /wp-admin/admin-ajax.php
	        url: ajaxurl,
	        // Add action and nonce to our collected data
	        data: $.extend(
	            {
	                ajax_shortcode_mastery_collection_table_nonce: $('#ajax_shortcode_mastery_collection_table_nonce').val(),
	                action: 'ajax_sm_shortcode_mastery_collection_table',
	            },
	            data
	        ),
	        beforeSend: function( ) {
	            $('#shortcodes-mastery-collection #the-list').empty();
	            $('#shortcodes-mastery-collection .tablenav').remove();
	            $('#shortcodes-mastery-collection .sm-no-items').remove();
	            $('#shortcodes-mastery-collection #sm-search-description').empty();
	            $('#shortcodes-mastery-collection #sm-pack-info').empty();
	            $('#shortcodes-mastery-collection #sm-group-info').empty();
	        },
			error: function() {
				$('#wp-admin-bar-sm-submit-top-button').find('div').html('<img class="sm-admin-bar-icon" src="'+ smajax.iconUrl + '">' + smajax.errorMessage);
				setTimeout("jQuery('#wp-admin-bar-sm-submit-top-button').find('div').fadeOut(300, function() { jQuery(this).text(''); jQuery(this).show(); });", 2800);
				$('#smLoaderObject').fadeOut(300);
				
				$('#shortcodes-mastery-collection #the-list').before( '<p class="sm-no-items">' + smajax.noItems + '</p>' );
				
			},
	        // Handle the successful result
	        success: function( response ) {
	            
	            $('#smLoaderObject').hide();
	            	            
	            try {
	            
	                var response = $.parseJSON( response );
	                
					insertContent( $, response, true );
										
				} catch( e ) {
					
					$('#shortcodes-mastery-collection #the-list').before( '<p class="sm-no-items">' + smajax.noItems + '</p>' );					
				}
				
				collection.init();
	            
	        }
	    });
    
	}
});

function bind_all() {
	
	bind_links();
	bind_animation();
}

window.unbind_current = function( id ) {
	
	$( 'a.sm-item-button[data-id=' + id + ']' ).unbind('click');
	
}

function bind_links() {	
			
	$('a.sm-item-button, a.sm-item-button-detailed').click( function( e ) {
						
		e.preventDefault();
				
		if ( in_progress ) return false;
		
		$('.notice').remove();
		
		in_progress = true;
		
		$(this).unbind('mouseenter mouseleave');

		if ( $(this).hasClass( 'sm-item-button' ) ) {
		
			var main = $(this);
			var id = main.attr('data-id');
			var plus = main.find('.plus');
			var add = main.find('.add');
			var detailed = null;
			main.unbind('click');
			
		} else {
			
			var detailed = $(this);
			var id = detailed.attr('data-id');		
			var main = $(window.parent.document).find( 'a.sm-item-button[data-id=' + id + ']' );
			var plus = main.find('.plus');
			var add = main.find('.add');
			detailed.unbind();
			detailed.attr('href', 'javascript:void(0);');
			window.parent.unbind_current( id );
			
		}
		
		main.attr('href', 'javascript:void(0);');

		add.css({opacity: 0});
		plus.stop().animate({opacity: 0}, 300, function() { plus.remove(); main.prepend('<i class="sm-refresh"></i>'); } );
		main.stop().animate({width: 56}, 300 );
		
		if ( id ) {
						
			if ( detailed ) detailed.html(smajax.shortcodeCreating);
			
			$('#wp-admin-bar-sm-submit-top-button').find('div').html('<img class="sm-admin-bar-icon" src="'+ smajax.iconUrl + '">' + smajax.shortcodeCreating);
						
			$.ajax({
				url: smajax.ajaxurl,
				type: 'POST',
				cache: true,
				timeout: 8000,
				data: {
					action: 'ajax_sm_add_shortcode',
					security: smajax.ajaxnonce,
					value: id
				},
				error: function() {
					$('#wp-admin-bar-sm-submit-top-button').find('div').html('<img class="sm-admin-bar-icon" src="'+ smajax.iconUrl + '">' + smajax.errorMessage);
					setTimeout("jQuery('#wp-admin-bar-sm-submit-top-button').find('div').fadeOut(300, function() { jQuery(this).text(''); jQuery(this).show(); });", 2800);
					
					setTimeout( function() {main.find('.sm-refresh').remove(); main.prepend('<span class="plus">+</span>'); },300);

					bind_animation();
					in_progress = false;
	
				},
				success: function(returnedData) {
					
					setTimeout( function() { main.find('.sm-refresh').remove(); add.css({opacity: '1'}); },300);
					main.removeAttr('data-id');
					
					var url = main.parent().find('a.details').attr('href');
					
					if ( url) {
						
						url = replaceUrlParam( url, 'disabled', 'true' );
						main.parent().find('a.image-thick, a.details').attr('href', url );
					}
					
					if ( detailed ) {
						detailed.removeAttr('data-id');
					}
					
					in_progress = false;
					
					try {
						
						var resp = JSON.parse(returnedData);
											
						if ( resp.error ) { 

							$('.shortcode-mastery-nav').prepend(resp.error);
							
							add.html(smajax.bad);
							main.css({'background': '#e74c3c', 'text-align': 'center'});
							main.animate({width: main.find('.add').width() + 42}, 300 );
							
							if ( detailed ) {
								detailed.html(smajax.bad);
								detailed.css({'background': '#e74c3c'});
							}
							
							$('#wp-admin-bar-sm-submit-top-button').find('div').html('<img class="sm-admin-bar-icon" src="'+ smajax.iconUrl + '">' + smajax.errorMessage);
							setTimeout("jQuery('#wp-admin-bar-sm-submit-top-button').find('div').fadeOut(300, function() { jQuery(this).text(''); jQuery(this).show(); });", 2800);					
					
						} else {
							
							add.html(smajax.added);
							main.attr('data-added', 'true');
							main.css({'background': '#46b450', 'text-align': 'center'});
							main.animate({width: main.find('.add').width() + 42}, 300 );
	
							if ( detailed ) {
								detailed.html(smajax.successMessageAdded);
								detailed.css({'background': '#46b450'});
							}
							
							$('#wp-admin-bar-sm-submit-top-button').find('div').html('<img class="sm-admin-bar-icon" src="'+ smajax.iconUrl + '">' + smajax.successMessageAdded);
							setTimeout("jQuery('#wp-admin-bar-sm-submit-top-button').find('div').fadeOut(300, function() { jQuery(this).text(''); jQuery(this).show(); });", 2800);						
						}
						
					} catch(e) {
						console.log(e);
					}
	
				}
				
			});
		
		}
				
	});
	
}

function bind_animation() {
	
	$('.sm-collection-item').find('.sm-lazy').hover(function() {
	
		$(this).stop().animate({opacity: '1', transform: 'scale(1.05)'}, 300 );
		
		}, function() {
	
		$(this).stop().animate({opacity: '1', transform: 'scale(1)'}, 300 );

	});
	
	$('a.sm-item-button').hover(function() {
								
		if ( $(this).attr('data-added') != 'true' ) $(this).stop().animate({width: $(this).find('.add').width() + 73}, 300 );
		
		}, function() {
	
		if ( $(this).attr('data-added') != 'true' ) $(this).stop().animate({width: 56}, 300 );

	});
}

var tb_position;
jQuery( document ).ready( function( $ ) {

	var tbWindowShortcode;
	
	var TB_WIDTH = 770;

	tb_position = function() {
		var width = $( window ).width(),
			H = $( window ).height() - 60,
			W = ( ( TB_WIDTH + 20 ) < width ) ? TB_WIDTH : width - 20;

		tbWindowShortcode = $( '.shortcodes_page_shortcode-mastery-templates #TB_window' );

		if ( tbWindowShortcode.length ) {
			tbWindowShortcode.width( W ).height( H );
			$( '.shortcodes_page_shortcode-mastery-templates iframe' ).width( W ).height( H );
			tbWindowShortcode.css({
				'margin-left': '-' + parseInt( ( W / 2 ), 10 ) + 'px'
			});
			if ( typeof document.body.style.maxWidth !== 'undefined' ) {
				tbWindowShortcode.css({
					'top': '30px',
					'margin-top': '0'
				});
			}
		}

		return $( '.shortcodes_page_shortcode-mastery-templates a.thickbox' ).each( function() {
			var href = $( this ).attr( 'href' );
			if ( ! href ) {
				return;
			}
			href = href.replace( /&width=[0-9]+/g, '' );
			href = href.replace( /&height=[0-9]+/g, '' );
			$(this).attr( 'href', href + '&width=' + W + '&height=' + ( H ) );
		});
	};

	$( window ).resize( function() {
		tb_position();
	});

});

/**
 * Ajax shortcodes collection search
 */	

(function($) {
	
var ajax_process;
 
search = {
 
    init: function() {
 
        // This will have its utility when dealing with the page number input
        var timer;
        var delay = 500;
 
		$('.sm-filter-search').keyup( function( e ) {

 
            // If user hit enter, we don't want to submit the form
            // We don't preventDefault() for all keys because it would
            // also prevent to get the page number!
            if ( 13 == e.which )
                e.preventDefault();
 
            // This time we fetch the variables in inputs
            var data = {
                paged: '1',
                tab: 'search',
				tag: '',
				pack: '',
				s: $('.sm-filter-search').val() || ''
            };
 
            // Now the timer comes to use: we wait half a second after
            // the user stopped typing to actually send the call. If
            // we don't, the keyup event will trigger instantly and
            // thus may cause duplicate calls before sending the intended
            // value
            window.clearTimeout( timer );
            timer = window.setTimeout(function() {
                search.update( data );
            }, delay);
        });
    },
 
    update: function( data ) {
	    
	    if ( ajax_process ) return;
	    
	    ajax_process = true;
	    
	    $('#smLoaderObject').fadeIn(300);
	    $('.tablenav').remove();
	    
        $.ajax({
            // /wp-admin/admin-ajax.php
            url: ajaxurl,
            // Add action and nonce to our collected data
            data: $.extend(
                {
                    ajax_shortcode_mastery_collection_table_nonce: $('#ajax_shortcode_mastery_collection_table_nonce').val(),
                    action: 'ajax_sm_shortcode_mastery_collection_table',
                },
                data
            ),
            
            beforeSend: function( ) {
	            $('#shortcodes-mastery-collection #the-list').empty();
	            $('#shortcodes-mastery-collection .tablenav').remove();
	            $('#shortcodes-mastery-collection .sm-no-items').remove();
	            $('#shortcodes-mastery-collection #sm-search-description').empty();
	            $('#shortcodes-mastery-collection #sm-pack-info').empty();
	            $('#shortcodes-mastery-collection #sm-group-info').empty();
            },
			error: function() {
				$('#wp-admin-bar-sm-submit-top-button').find('div').html('<img class="sm-admin-bar-icon" src="'+ smajax.iconUrl + '">' + smajax.errorMessage);
				setTimeout("jQuery('#wp-admin-bar-sm-submit-top-button').find('div').fadeOut(300, function() { jQuery(this).text(''); jQuery(this).show(); });", 2800);
				$('#smLoaderObject').fadeOut(300);
				
				$('#shortcodes-mastery-collection #the-list').before( '<p class="sm-no-items">' + smajax.noItems + '</p>' );
				
				ajax_process = false;
			},
            // Handle the successful result
            success: function( response ) {
	            
	            $('#smLoaderObject').hide();
                 
                try { 
	                
	                var response = $.parseJSON( response );
	                                	 
	                insertContent( $, response );
	                										
				} catch( e ) {
					
					$('#shortcodes-mastery-collection #the-list').before( '<p class="sm-no-items">' + smajax.noItems + '</p>' );
					
				}
				
				var searchLi = '<li>' + '<a href="javascript:void(0);" class="current" data-tab="search">' + smajax.searchTitle + '</a>' + '</li>';
				
				var notSearch = true;
				$('.sm-filter-links').find('a').not('a[data-tab="search"]').removeClass('current');
				$('.sm-filter-links').find('a').each( function() {
					if ( $(this).attr('data-tab') == 'search' ) notSearch = false;
				});
				if ( notSearch ) $('.sm-filter-links').prepend( searchLi );
				
                collection.init();
                
                ajax_process = false;
            }
        });
    },
 
    /**
     * Filter the URL Query to extract variables
     * 
     * @see http://css-tricks.com/snippets/javascript/get-url-variables/
     * 
     * @param    string    query The URL query part containing the variables
     * @param    string    variable Name of the variable we want to get
     * 
     * @return   string|boolean The variable value if available, false else.
     */
    __query: function( query, variable ) {
 
        var vars = query.split("&");
        for ( var i = 0; i <vars.length; i++ ) {
            var pair = vars[ i ].split("=");
            if ( pair[0] == variable )
                return pair[1];
        }
        return false;
    },
}

search.init();
 
})(jQuery);

/**
 * Ajax shortcodes collection pagination
 */	

(function($) {
	
var ajax_process;
 
collection = {
 
    init: function() {
 
        // This will have its utility when dealing with the page number input
        var timer;
        var delay = 500;
 
        // Pagination links, sortable link
        $('#shortcodes-collection-filter .tablenav-pages a').on('click', function(e) {
            // We don't want to actually follow these links
            e.preventDefault();
            // Simple way: use the URL to extract our needed variables
            var query = this.search.substring( 1 );
             
            var data = {
                paged: collection.__query( query, 'paged' ) || '1',
				tab: collection.__query( query, 'tab' ) || 'all',
				tag: collection.__query( query, 'tag' ) || '',
				pack: collection.__query( query, 'pack' ) || '',
				s: collection.__query( query, 's' ) || '',
            };
            collection.update( data );
        });
 
        // Page number input
        $('#shortcodes-collection-filter input[name=paged]').on('keyup', function(e) {
 
            // If user hit enter, we don't want to submit the form
            // We don't preventDefault() for all keys because it would
            // also prevent to get the page number!
            if ( 13 == e.which )
                e.preventDefault();
 
            // This time we fetch the variables in inputs
            var data = {
                paged: parseInt( $('#shortcodes-collection-filter input[name=paged]').val() ) || '1',
                tab: $('.sm-filter-links a.current').attr('data-tab') || 'all',
				tag: $('.sm-tags a.current').attr('data-tag') || '',
				pack: $('.sm-pack a.current').attr('data-pack') || '',
				s: $('.sm-filter input[name=s]').val() || ''
            };
 
            // Now the timer comes to use: we wait half a second after
            // the user stopped typing to actually send the call. If
            // we don't, the keyup event will trigger instantly and
            // thus may cause duplicate calls before sending the intended
            // value
            window.clearTimeout( timer );
            timer = window.setTimeout(function() {
                collection.update( data );
            }, delay);
        });
    },
 
    update: function( data ) {
	    
	    if ( ajax_process ) return;
	    
	    ajax_process = true;
	    
	    $('#smLoaderObject').fadeIn(300);
	    $("html, body").animate({ scrollTop: 0 });
	    
        $.ajax({
            // /wp-admin/admin-ajax.php
            url: ajaxurl,
            // Add action and nonce to our collected data
            data: $.extend(
                {
                    ajax_shortcode_mastery_collection_table_nonce: $('#ajax_shortcode_mastery_collection_table_nonce').val(),
                    action: 'ajax_sm_shortcode_mastery_collection_table',
                },
                data
            ),
            beforeSend: function( ) {
	            $('#shortcodes-mastery-collection #the-list').empty();
	            $('#shortcodes-mastery-collection .tablenav').remove();
	            $('#shortcodes-mastery-collection .sm-no-items').remove();
	            $('#shortcodes-mastery-collection #sm-search-description').empty();
	            $('#shortcodes-mastery-collection #sm-pack-info').empty();
	            $('#shortcodes-mastery-collection #sm-group-info').empty();
            },
			error: function() {
				$('#wp-admin-bar-sm-submit-top-button').find('div').html('<img class="sm-admin-bar-icon" src="'+ smajax.iconUrl + '">' + smajax.errorMessage);
				setTimeout("jQuery('#wp-admin-bar-sm-submit-top-button').find('div').fadeOut(300, function() { jQuery(this).text(''); jQuery(this).show(); });", 2800);
				$('#smLoaderObject').fadeOut(300);
				
				$('#shortcodes-mastery-collection #the-list').before( '<p class="sm-no-items">' + smajax.noItems + '</p>' );
				
				ajax_process = false;
			},
            // Handle the successful result
            success: function( response ) {
	            
	            $('#smLoaderObject').hide();
	            	            
	            try {
                
	                var response = $.parseJSON( response );
	                
	                insertContent( $, response );
	                										
				} catch( e ) {
					
					$('#shortcodes-mastery-collection #the-list').before( '<p class="sm-no-items">' + smajax.noItems + '</p>' );					
				}

                collection.init();
                
                ajax_process = false;
            }
        });
    },
 
    /**
     * Filter the URL Query to extract variables
     * 
     * @see http://css-tricks.com/snippets/javascript/get-url-variables/
     * 
     * @param    string    query The URL query part containing the variables
     * @param    string    variable Name of the variable we want to get
     * 
     * @return   string|boolean The variable value if available, false else.
     */
    __query: function( query, variable ) {
 
        var vars = query.split("&");
        for ( var i = 0; i <vars.length; i++ ) {
            var pair = vars[ i ].split("=");
            if ( pair[0] == variable )
                return pair[1];
        }
        return false;
    },
}

collection.init();
 
})(jQuery);

function insertContent( $, response, flag ) {
			                
	$('.tablenav').remove();
	 
    if ( response.rows.length )
        $('#shortcodes-mastery-collection #the-list').html( response.rows );

    if ( response.pagination.bottom.length ) {
                           
        $('#shortcodes-mastery-collection #the-list').after( response.pagination.bottom );
        $('#shortcodes-mastery-collection .tablenav').css({ top: 50, opacity: 0, position: 'relative' }); 
        $('#shortcodes-mastery-collection .tablenav').animate({ top: 0, opacity: 1 }, 200 );
        
    }
    
    if ( response.pagination.top.length ) {
                            
        $('#shortcodes-mastery-collection #the-list').before( response.pagination.top );
        $('#shortcodes-mastery-collection .tablenav').css({ top: 50, opacity: 0, position: 'relative' }); 
        $('#shortcodes-mastery-collection .tablenav').animate({ top: 0, opacity: 1 }, 200 );
        
	}
	
	if ( response.searchDescription.length ) {
	
		$('#shortcodes-mastery-collection #sm-search-description').html( response.searchDescription );
		$('#shortcodes-mastery-collection #sm-search-description').css({ top: 50, opacity: 0, position: 'relative' });
		$('#shortcodes-mastery-collection #sm-search-description').animate({ top: 0, opacity: 1 }, 200 );
		
	}
		
	if ( response.packInfo.length ) {
	
		$('#shortcodes-mastery-collection #sm-pack-info').html( response.packInfo );
		$('#shortcodes-mastery-collection #sm-pack-info').css({ top: 50, opacity: 0, position: 'relative' });
		$('#shortcodes-mastery-collection #sm-pack-info').animate({ top: 0, opacity: 1 }, 200 );
		
	}
		
	if ( response.groupInfo.length ) {
	
		$('#shortcodes-mastery-collection #sm-group-info').html( response.groupInfo );
		$('#shortcodes-mastery-collection #sm-group-info').css({ top: 50, opacity: 0, position: 'relative' });
		$('#shortcodes-mastery-collection #sm-group-info').animate({ top: 0, opacity: 1 }, 200 );
		
	}
	
	if ( ! flag ) {	

		$('#the-list').masonry();
		$('#the-list').masonry('remove');
		$('#the-list').masonry('destroy');
		
	}
	
	$('#the-list').masonry({
		itemSelector : '.sm-collection-item',
		isFitWidth: false,
		percentPosition: true,
	});
	
	window.sr = ScrollReveal();

	var srIndex = 0,
	    srLastPos = 0,
	    srCurrentPos;
	    
	sr.reveal('.sm-collection-item', { 
		duration: 300, 
		origin: 'bottom', 
		scale: 1,
		distance: '100px',
		beforeReveal: function (domEl) {
	        srCurrentPos = domEl.offsetTop;
	
	        if (srLastPos == srCurrentPos) {
	            srIndex++;
	        }
	        else {
	            srIndex = 0;
	        }
	
	        srLastPos = srCurrentPos;
	        sr.reveal(domEl, {delay: srIndex * 50});
	    },
		afterReveal: function (domEl) {
			$(domEl).find('img').attr( 'src', $(domEl).find('img').attr( 'data-original' ) );
			setTimeout( function() { jQuery(domEl).imagesLoaded().progress( onProgress ); }, 200);
		}
	});
    
    bind_all();
}

function replaceUrlParam(url, paramName, paramValue){
    if(paramValue == null)
        paramValue = '';
    var pattern = new RegExp('\\b('+paramName+'=).*?(&|$)')
    if(url.search(pattern)>=0){
        return url.replace(pattern,'$1' + paramValue + '$2');
    }
    return url + (url.indexOf('?')>0 ? '&' : '?') + paramName + '=' + paramValue 
}

function getAllUrlParams(url) {

  // get query string from url (optional) or window
  var queryString = url ? url.split('?')[1] : window.location.search.slice(1);

  // we'll store the parameters here
  var obj = {};

  // if query string exists
  if (queryString) {

    // stuff after # is not part of query string, so get rid of it
    queryString = queryString.split('#')[0];

    // split our query string into its component parts
    var arr = queryString.split('&');

    for (var i=0; i<arr.length; i++) {
      // separate the keys and the values
      var a = arr[i].split('=');

      // in case params look like: list[]=thing1&list[]=thing2
      var paramNum = undefined;
      var paramName = a[0].replace(/\[\d*\]/, function(v) {
        paramNum = v.slice(1,-1);
        return '';
      });

      // set parameter value (use 'true' if empty)
      var paramValue = typeof(a[1])==='undefined' ? true : a[1];

      // (optional) keep case consistent
      paramName = paramName.toLowerCase();
      paramValue = typeof paramValue === 'string' ? paramValue.toLowerCase() : true;

      // if parameter name already exists
      if (obj[paramName]) {
        // convert value to array (if still string)
        if (typeof obj[paramName] === 'string') {
          obj[paramName] = [obj[paramName]];
        }
        // if no array index number specified...
        if (typeof paramNum === 'undefined') {
          // put the value on the end of the array
          obj[paramName].push(paramValue);
        }
        // if array index number specified...
        else {
          // put the value at that index number
          obj[paramName][paramNum] = paramValue;
        }
      }
      // if param name doesn't exist yet, set it
      else {
        obj[paramName] = paramValue;
      }
    }
  }

  return obj;
}

function onProgress( imgLoad, image ) {

	var $item = $( image.img ).parent().parent();
	$item.removeClass('loading');
	
	if ( !image.isLoaded ) {
		$item.addClass('broken');
	}
	
}